using Microsoft.VisualStudio.TestTools.UnitTesting;
using palikohrnne_web_app;
using palikohrnne_web_app.Api;
using palikohrnne_web_app.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using palikohrnne_web_app.Api;
using palikohrnne_web_app.Extensions;
using palikohrnne_web_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebappTest
{
    [TestClass]
    public class CitoyenConnecteTest
    {
        private readonly CubesService _cubesService;
        public CitoyenConnecteTest()
        { 
            _cubesService = new CubesService(new System.Net.Http.HttpClient());
            
        }

        public async Task SeConnecter()
        {
            var loginModel = await _cubesService.SeConnecter(new Citoyen
            {
                Mail = "valentin@gmail.com",
                MotDePasse = "1234"                
            });

            _cubesService.RenseignerToken(loginModel.token);
        }

        [TestMethod]
        public async Task Commenter()
        {
            await SeConnecter();

            HttpResponseMessage httpResponse = await _cubesService.CreateCommentaire(new Commentaire
            {
                CitoyenID = 3,
                Contenu = "Commentaire",
                RessourceID = 1,
            });
            //Doit �tre connect�
            Assert.IsTrue(httpResponse.IsSuccessStatusCode);
        }
        [TestMethod]
        public async Task ModifierRessource()
        {
            await SeConnecter();
            Ressource res = await _cubesService.GetRessourceById(1);
            res.Contenu = "test test";
            HttpResponseMessage httpResponse = await _cubesService.UpdateRessource(res);

            httpResponse = await _cubesService.UpdateRessource(res);
            Assert.IsTrue(httpResponse.IsSuccessStatusCode);
        }
    }
}
