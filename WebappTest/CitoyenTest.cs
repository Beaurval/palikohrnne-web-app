using Microsoft.VisualStudio.TestTools.UnitTesting;
using palikohrnne_web_app;
using palikohrnne_web_app.Api;
using palikohrnne_web_app.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebappTest
{
    [TestClass]
    public class CitoyenTest
    {
        private readonly CubesService _cubesService;
        public CitoyenTest()
        {
            _cubesService = new CubesService(new System.Net.Http.HttpClient());
        }

        [TestMethod]
        public async Task ListerRessource()
        {
            IEnumerable<Ressource> ressources = await _cubesService.GetAllRessources();
            Assert.IsNotNull(ressources.ToList().Count);
            Assert.AreNotEqual(0, ressources.ToList().Count);
        }
        [TestMethod]
        public async Task CreerCitoyen()
        {
           HttpResponseMessage responseMessage = await _cubesService.CreateCitoyen(new Citoyen { 
                Adresse = "test",
                CodePostal = "76450",
                Genre = "H",
                Mail = "mail@monmail.fr",
                MotDePasse = "MotDePasse",
                Nom = "Nom",
                Prenom = "Prenom",
                Pseudo = "Pseudo",
                Rang = await  _cubesService.GetRangById(1),
                RangID = 1 ,
                Telephone = "023598775",
                Ville = "Ville"
            });
           
            Assert.IsTrue(responseMessage.IsSuccessStatusCode);
        }
        [TestMethod]
        public async Task Commenter()
        {
            HttpResponseMessage httpResponse =await _cubesService.CreateCommentaire(new Commentaire
            {
                CitoyenID = 3,
                Contenu = "Commentaire",
                RessourceID = 1,
            });
            //Doit tre connect
            Assert.IsFalse(httpResponse.IsSuccessStatusCode);
        }
    }
}

