FROM $DOCKERIMAGE

WORKDIR /app
COPY /publish .

EXPOSE 5000/tcp

ENV ASPNETCORE_URLS "http://*:5000"
ENV ASPNETCORE_ENVIRONMENT "$ENVIRONMENT"
CMD ["dotnet", "$ENTRYPOINT"]